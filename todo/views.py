from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from .models import *
from .forms import *

# Create your views here.
def index(request):
    tasks=Task.objects.all()
    form = TaskForm()

    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid:
            form.save()
            html_content = render_to_string('todo_tasks/email_template.html',
            {'title':'New Task Added','line':'A New Task has been added','content':form['title'].value()})
            # return HttpResponse(str(html_content))
            text_content = strip_tags(html_content)
            email = EmailMultiAlternatives(
                'New Task Added',
                text_content,
                'admin@todo.com',
                ['afifa@example.com'],
            )
            email.attach_alternative(html_content,"text/html")
            email.send()

            return redirect('/')

    data={'tasks': tasks, 'form' : form}
    return render(request, 'todo_tasks/index.html', data)

def update(request, task_id):
        task= Task.objects.get(id=task_id)

        form = TaskForm(instance = task)

        if request.method == 'POST':
         form = TaskForm(request.POST, instance = task)
         if form.is_valid:
            form.save()
            html_content = render_to_string('todo_tasks/email_template.html', {'title':'Task Updated','line':'A Task has been updated','content':form['title'].value()})
            text_content = strip_tags(html_content)
            email = EmailMultiAlternatives(
                'Task Updated',
                text_content,
                'admin@todo.com',
                ['afifa@example.com'],)
            email.attach_alternative(html_content,"text/html")
            email.send()

            return redirect('/')

        
        data={'form': form}
        return render(request, 'todo_tasks/update.html', data)

def delete(request, task_id):
            task= Task.objects.get(id=task_id)

            if request.method == "POST":
                 html_content = render_to_string('todo_tasks/email_template.html',
            {'title':'Task Deleted','line':'A Task has been deleted','content':task.title})
            text_content = strip_tags(html_content)
            email = EmailMultiAlternatives(
            'Task Deleted',
            text_content,
            'admin@todo.com',
            ['afifa@example.com'],)
            email.attach_alternative(html_content,"text/html")
            email.send()
            task.delete()
            return redirect('/')

            data={'task_name': task}
            return render(request, 'todo_tasks/delete.html', data)
