from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='all_tasks'),
    path('update/<str:task_id>/', views.update, name="update_task"),
    path('delete/<str:task_id>/', views.delete, name="delete_task"),
]